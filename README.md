# NixOS Composition for Ryax

This repository contains compositions for testing [Ryax](https://github.com/ryaxtech/ryax-engine) with [NixOS Compose a.k.a. NXC](https://nixos-compose.gitlabpages.inria.fr/nixos-compose/index.html).

> WARNING: Running on Docker or VMs is not supported yet because of some NXC
> limitations

## Run on Grid5000

Connect to a Grid5000 site and clone this repository.

To do so, connect to the frontend of a site and install NixOS-compose (a.k.a `nxc`) to be able to run the composition build:
```sh
pip install nixos-compose==0.6.0

# You might want to add this on your .bashrc
cat >> ~/.bashrc <<EOF
export PATH=$PATH:$HOME/.local/bin
EOF

# Make the exeutable available
source ~/.bashrc

nxc helper install-nix

# Add some nix configuration
mkdir -p ~/.config/nix
cat > ~/.config/nix/nix.conf <<EOF
experimental-features = nix-command flakes
EOF

nix --version
```

> NOTE:
> Because building the environment might use a lot of resources it is advised to run this build inside an interactive job using:
> `oarsub -I`

Now that you have Nix installed, and the `nxc` available and you can build environment:
```sh
cd ryax-nxc-compositions/ryax
nxc build -C oar::g5k-nfs-store
```

Finally, you can reserve some resources and deploy:
```sh
# Get some resource and capture the Job ID
export $(oarsub -l cluster=1/nodes=3,walltime=1:0:0 "$(nxc helper g5k_script) 12h" | grep OAR_JOB_ID)
# WAit until the job starts...
oarstat -j $OAR_JOB_ID -J | jq --raw-output 'to_entries | .[0].value.assigned_network_address | .[]' > machines
nxc start -C oar::g5k-nfs-store -m machines
```

In order to expose Ryax WebUI on your machine, you can create a port forward. Run this on your machine but replace the nodes and the site. To the node IP by name, for example for server use `nxc helper ip server`:
```sh
ssh -f -N <SITE>.g5k -L 8081:<SERVER MACHINE>:80
```

