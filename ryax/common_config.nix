{ pkgs, ...}:
{
  environment.systemPackages = with pkgs; [
    python3
    vim
    cpufrequtils
    htop
    tree
    cri-tools
    jq
    gzip
    kubectl
    ripgrep
  ];

  networking.firewall.enable = false;

  environment.noXlibs = false;

  environment.shellAliases = {
    k = "k3s kubectl";
    kubectl = "k3s kubectl";
    kgp = "k3s kubectl get pods -A";
  };

  nxc.users = { names = [ "user1" "user2" ]; prefixHome = "/users"; };

  security.pam.loginLimits = [
    { domain = "*"; item = "memlock"; type = "-"; value = "unlimited"; }
    { domain = "*"; item = "stack"; type = "-"; value = "unlimited"; }
  ];
}
