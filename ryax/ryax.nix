{ lib, pkgs, config, ... }:
with lib;
let cfg = config.services.ryax-install;
in {
  options.services.ryax-install = {
    enable = mkEnableOption "ryax-install service";
    kubeconfigPath = mkOption {
      type = types.path;
      default = "/etc/ryax/kubeconfig.yaml";
    };
    ryaxVersion = mkOption {
      type = types.string;
      default = "24.02.0";
    };
    ryaxAdmVersion = mkOption {
      type = types.string;
      default = cfg.ryaxVersion;
    };
    ryaxConfigFile = mkOption {
      type = types.path;
      default = pkgs.writeText "values.yaml" ''
        version: 24.02.0
        clusterName: local
        imagePullPolicy: Always
        logLevel: debug
        storageClass: nfs-csi
        monitoring:
          enabled: false
        environment: development
        tls:
          enabled: false

        datastore:
          pvcSize: 2Gi
        filestore:
          pvcSize: 5Gi
        registry:
          pvcSize: 5Gi

        traefik:
          enabled: false
        prometheus:
          enabled: false
        loki:
          enabled: true
        rabbitmq:
          values:
            metrics:
              enabled: false
            ulimitNofiles: ""
        certManager:
          enabled: false
      '';
    };
  };

  config = mkIf cfg.enable {
    systemd.services.ryax-install = {
      after = [ "k3s.service" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        # Type = "oneshot";
        Restart = "on-failure";
        RestartSec = 1;
      };
      path = [
        pkgs.kubectl
        pkgs.gitMinimal
        pkgs.poetry
        (pkgs.wrapHelm pkgs.kubernetes-helm { plugins = [ pkgs.kubernetes-helmPlugins.helm-diff ]; })
        pkgs.helmfile
        pkgs.bash
      ];
      script = ''
        set -e
        set -x
        set -u

        export KUBECONFIG=${cfg.kubeconfigPath}

        echo Waiting for Kubernetes API...
        until [[ $(kubectl get endpoints/kubernetes -n default -o=jsonpath='{.subsets[*].addresses[*].ip}' ) ]]; do sleep 2; echo -n .; done
        echo Kubernetes Connection is UP!
        until [[ $(kubectl get nodes | grep ' Ready') ]]; do sleep 2; echo -n .; done
        echo Kubernetes node is Ready!


        helm repo add csi-driver-nfs https://raw.githubusercontent.com/kubernetes-csi/csi-driver-nfs/master/charts
        helm install csi-driver-nfs csi-driver-nfs/csi-driver-nfs --namespace kube-system --version v4.5.0
        mkdir -p /users/k8s
        cat > k3s-nfs-pvc.yaml <<EOF
        apiVersion: storage.k8s.io/v1
        kind: StorageClass
        metadata:
          name: nfs-csi
        provisioner: nfs.csi.k8s.io
        parameters:
          server: server
          share: /users/k8s
        reclaimPolicy: Delete
        volumeBindingMode: Immediate
        EOF
        kubectl apply -f k3s-nfs-pvc.yaml
        kubectl patch storageclass nfs-csi -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
        kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}' | true

        RYAX_ADM_DIR=$(mktemp -d)
        git clone https://gitlab.com/ryax-tech/ryax/ryax-adm.git $RYAX_ADM_DIR

        cd $RYAX_ADM_DIR
        git checkout ${cfg.ryaxAdmVersion}
        poetry install --without dev
        poetry run ryax-adm apply --values ${cfg.ryaxConfigFile} --retry 2 --suppress-diff
      '';
    };
  };
}
