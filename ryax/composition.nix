{ pkgs, ... }: {
  roles = let
    tokenFile = pkgs.writeText "token" "p@s$w0rdToNotUseInProd";
    in {
    server = { config, pkgs, ... }:
    {
      imports = [ ./ryax.nix ./common_config.nix ];
      nxc.sharedDirs."/users".export = true;

      system.activationScripts.k3s-config = ''
          # WARNING: Does not work with docker flavor
          SERVER=$( grep server /etc/nxc/deployment-hosts | ${pkgs.gawk}/bin/awk '{ print $1 }')
          echo 'bind-address: "'$SERVER'"' >> /etc/k3s.yaml
          echo 'node-external-ip: "'$SERVER'"' >> /etc/k3s.yaml
        '';

      services.k3s = {
        inherit tokenFile;
        enable = true;
        role = "server";
        configPath = "/etc/k3s.yaml";
        environmentFile = pkgs.writeText "k3s-export" ''
          K3S_KUBECONFIG_OUTPUT=/etc/ryax/kubeconfig.yaml
          K3S_KUBECONFIG_MODE=666
        '';
      };

      services.ryax-install.enable = true;
      services.ryax-install.ryaxAdmVersion = "24.06.0-dev1";
    };

    node = { config, pkgs, ... }:
    {
      imports = [ ./common_config.nix ];
      nxc.sharedDirs."/users".server = "server";

      services.k3s = {
        inherit tokenFile;
        enable = true;
        role = "agent";
        serverAddr = "https://server:6443";
      };
    };
  };
  rolesDistribution = { node = 2; };
  testScript = ''
    server.wait_for_unit('k3s.service')
    server.wait_until_succeeds('k3s kubectl get nodes | grep Ready', timeout=10)
    # This can take some time depending on your network connection
    server.wait_until_succeeds('k3s kubectl get pods -A | grep Running', timeout=90)

    server.wait_for_unit('ryax-install.service')
    server.succeed('curl http://localhost/app/')
    log("🚀 BeBiDa with OAR, K3s and Ryax is up and running!")
  '';
}
